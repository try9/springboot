package com.barn.springboot.dao;

import com.barn.springboot.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

public interface UserDao {

    int deleteByPrimaryKey(Integer userId);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    //这个方式我自己加的
    List<User> selectAllUser();
}