package com.barn.springboot.service.impl;

import com.barn.springboot.dao.UserDao;
import com.barn.springboot.entity.User;
import com.barn.springboot.service.UserService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zhangzl on 18-8-10.
 */
@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public int addUser(User user) {
		return userDao.insertSelective(user);
	}

	/*
	 * 这个方法中用到了我们开头配置依赖的分页插件pagehelper
	 * 很简单，只需要在service层传入参数，然后将参数传递给一个插件的一个静态方法即可；
	 * pageNum 开始页数
	 * pageSize 每页显示的数据条数
	 * */
	@Override
	public List<User> findAllUser(int pageNum, int pageSize) {
		//将参数传给这个方法就可以实现物理分页了，非常简单。
		PageHelper.startPage(pageNum, pageSize);
		return userDao.selectAllUser();
//		return null;
	}
}
