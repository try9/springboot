package com.barn.springboot.service;

import com.barn.springboot.entity.User;

import java.util.List;

/**
 * Created by zhangzl on 18-8-10.
 */
public interface UserService {

	int addUser(User user);

	List<User> findAllUser(int pageNum, int pageSize);
}